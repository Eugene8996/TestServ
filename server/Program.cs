﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text.Json;

namespace server
{
    class MainClass
    {
        static void Main(string[] args)
        {
            ReceiveClass receiveClass = new ReceiveClass();
            receiveClass.ReceiveData();
        }
    }

    class CoordClass
    {
        public int Coord_X { get; set; }
        public int Coord_Y { get; set; }
    }

    class ReceiveClass
    {
        static int port = 8005; // порт для приема входящих запросов
        public void ReceiveData()
        {
            // получаем адреса для запуска сокета
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);

            // создаем сокет
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                // связываем сокет с локальной точкой, по которой будем принимать данные
                listenSocket.Bind(ipPoint);

                // начинаем прослушивание
                listenSocket.Listen(10);

                List<int[]> data_rec = new List<int[]>();

                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    Socket handler = listenSocket.Accept();
                    // получаем сообщение
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0; // количество полученных байтов
                    byte[] data = new byte[256]; // буфер для получаемых данных
                    string data_rec_char;

                    do
                    {
                        bytes = handler.Receive(data);
                        data_rec_char = Encoding.Unicode.GetString(data, 0, bytes);
                        Console.WriteLine("handler");
                    }
                    while (handler.Available > 0);


                    Console.WriteLine(data_rec_char);

                    List<CoordClass> recive_data = JsonSerializer.Deserialize<List<CoordClass>>(data_rec_char);

                    foreach (CoordClass coord in recive_data)
                        Console.WriteLine(coord.Coord_X + " " + coord.Coord_Y);

                    // отправляем ответ
                    string message = "ваше сообщение доставлено";
                    data = Encoding.Unicode.GetBytes(message);
                    handler.Send(data);
                    // закрываем сокет
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    class ParserClass
    {
        private static readonly string block_start = "start";
        private static readonly string block_end = "###END";
        private static readonly UInt16 number_coordinates = 2;

        private int count_coordinates = 0;
        private string buf;
        List<int[]> data_to_send;

        public List<int[]> Pars(StreamReader reader)
        {
            data_to_send = new List<int[]>();
            string[] Separators = new string[] { " , " };
            int[] coordinates = new int[number_coordinates];
            do
            {
                buf = reader.ReadLine();
            } while (buf != block_start);

            buf = reader.ReadLine();
            while (buf != block_end)
            {
                string[] coordinates_char = buf.Split(Separators, StringSplitOptions.None);

                data_to_send.Add(new int[number_coordinates]);
                data_to_send[count_coordinates][0] = Convert.ToInt32(coordinates_char[0]);
                data_to_send[count_coordinates][1] = Convert.ToInt32(coordinates_char[1]);
                count_coordinates++;
                buf = reader.ReadLine();
            }

            return data_to_send;
        }
    }

}
